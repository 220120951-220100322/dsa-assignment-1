import ballerina/io;
import ballerina/http;

type Student record{|
    string FirstName;
    string LastName;
    int StudentNumber;
    string LearningMaterial;
    string PreviousSubjects;

|}

Student[] StudentProfile = [];

service /students on new http:Listener(9090) {
    resource function get All() returns Student[] {
        io:println("handling GET request to /students/all");
        return StudentProfile;  
        }
        resource function get All() returns json{
            io:prntln("Handling GET request to /students/students{StudentNumber}/all");
            return Student{StudentNumber};
        }
        resource function get All() returns json{
            io:println("Handling GET request to /Learning Materials/all");
            return LearningMaterials + PreviousSubjects;
        }

    resource function post insert(@http:Payload Student newLearner) returns json{
        io:println("Handling POST request to /users/insert");
        StudentProfile.push(newLearner);
        return{done: "Successfully added new user!"};
    }
    resource function get one/[string FirstName]/[string LastName]/[int StudentNumber]/[string LearningMaterials]/[string PreviousSubjects] () returns json{
        return{First_Name: FirstName, Last_Name: LastName, Student_Number: StudentNumber, Learning_Materials: LearningMaterials, Precious_Subjects: PreviousSubjects};
    }
}
