import ballerina/grpc;

public isolated client class functionOperationsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_new_fnContext(function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function add_fns(function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_fnsContext(function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function delete_fn(function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function delete_fnContext(function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_fn(function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_fnContext(function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_all_fns(function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_all_fnsContext(function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria(function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_all_with_criteriaContext(function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionOperations/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }
}

public client class FunctionOperationsStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFunction record {|
    function content;
    map<string|string[]> headers;
|};

public type function
record {|string fullName = ""; string email = ""; string programmingLanguage = ""; string keywords = "";|} ;
const string ROOT_DESCRIPTOR = "0A1772656D6F74655F6F7065726174696F6E732E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F228A010A0866756E6374696F6E121A0A0866756C6C4E616D65180120012809520866756C6C4E616D6512140A05656D61696C1802200128095205656D61696C12300A1370726F6772616D6D696E674C616E6775616765180320012809521370726F6772616D6D696E674C616E6775616765121A0A086B6579776F72647318042001280952086B6579776F72647332E5020A1266756E6374696F6E4F7065726174696F6E7312350A0A6164645F6E65775F666E12092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A076164645F666E7312092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512340A0964656C6574655F666E12092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A0773686F775F666E12092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512370A0C73686F775F616C6C5F666E7312092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512410A1673686F775F616C6C5F776974685F637269746572696112092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "remote_operations.proto": "0A1772656D6F74655F6F7065726174696F6E732E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F228A010A0866756E6374696F6E121A0A0866756C6C4E616D65180120012809520866756C6C4E616D6512140A05656D61696C1802200128095205656D61696C12300A1370726F6772616D6D696E674C616E6775616765180320012809521370726F6772616D6D696E674C616E6775616765121A0A086B6579776F72647318042001280952086B6579776F72647332E5020A1266756E6374696F6E4F7065726174696F6E7312350A0A6164645F6E65775F666E12092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A076164645F666E7312092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512340A0964656C6574655F666E12092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A0773686F775F666E12092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512370A0C73686F775F616C6C5F666E7312092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512410A1673686F775F616C6C5F776974685F637269746572696112092E66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

