import ballerina/grpc;
import ballerina/lang.value as value1 ;
import ballerina/log;
listener grpc:Listener ep = new (9090);

function [] functionList = [];
//converted functions to arrays

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "functionOperations" on ep {

    remote function add_new_fn(function value) returns string|error {
        functionList.push(value);
        return "New function added";

    //this function adds a new function
    }
    remote function add_fns(function value) returns string|error {
        functionList.push(value);
        return "New functions added";
        //this array adds muptiple functions.
        
    }
    remote function delete_fn(function value) returns string|error {
        functionList.filter(value);
        return "Function deleted";

        //this array deletes a function

    }
    remote function show_fn(function value) returns string|error {
        io:println(functionList);
        return "Here's the function of the version you requested";
        //this array displays functions
    }
    remote function show_all_fns(function value) returns string|error {
        io:println(functionList);
        return "Here's the function with all versions";

        //this array displays funtions of all versions
    }
    remote function show_all_with_criteria(function value) returns string|error {
        io:println(functionList);
        return "Here are the functions that meet your prerequisites";
            //this array displays certain functions
        
    }
}

